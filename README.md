### How to run this project  

**Open the following link:**  
https://play.nativescript.org/?template=play-vue&id=eIFan9&v=3  

**Install both Nativescript Playground and Nativescript Preview on an iOS device.**  
**Use the device to read the QR code displayed on the page.**
